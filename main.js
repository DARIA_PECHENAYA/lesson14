// let arr = [1,2,3];
// let result=arr.map((item)=>
// {return item*2;});
// console.log(result);

// let arr = [1,2,3];
// let test = arr;
// test.push(4);
// console.log(test);
// console.log(arr);

// let user1 = {
//     name: "test",
//     lastName: "last"
// };
// let user2 = Object.assign( {}, user1);
// user2.name = "123";
// console.log(user1.name);
// console.log(user2.name);

/* копирование изменением*/ 

let user1 = {
    name: "test",
    lastName: "last"
};
let user2 = user1;
user2.name = "123";
console.log(user1.name);
console.log(user2.name);

// const fruits = [ 'apple', 'peer', 'orange', 'grapes', 'banana' ];
// const newFruits = fruits;
// console.log(fruits);
// console.log(newFruits);

const fruits = [ 'apple', 'peer', 'orange', 'grapes', 'banana' ];
const newFruits = fruits.slice();
console.log(fruits);
console.log(newFruits);

// let vegetables = ['капустка','репка','люк - я твой отец'];
// console.log(vegetables.join());

let vegetables = ['капустка','репка','люк - я твой отец'];
let newVegetables = vegetables.toString();
console.log(newVegetables);

let nums = [1,2,3,4,5];
const reducer = (accumulator, currentValue) => accumulator + currentValue;
console.log(nums.reduce(reducer));

let languages = [ 'script', 'script', 'php', 'php', 'html', 'html', 'css', 'css', 'java' ];

const result=[];
languages.forEach( ( item ) => {
    if( !result.find ( value => item === value) ) result.push(item);
});
console.log(result);

// let newLanguages = [new Set(languages)];
// console.log(newLanguages);

let reaturningNums3 = [3,3,7,7,3,3,4,5,5,8,8,8].filter((item) => {return item === 3}).length;
console.log(reaturningNums3);
let reaturningNums7 = [3,3,7,7,3,3,4,5,5,8,8,8].filter((item) => {return item === 7}).length;
console.log(reaturningNums7);
let reaturningNums4 = [3,3,7,7,3,3,4,5,5,8,8,8].filter((item) => {return item === 4}).length;
console.log(reaturningNums4);
let reaturningNums5 = [3,3,7,7,3,3,4,5,5,8,8,8].filter((item) => {return item === 5}).length;
console.log(reaturningNums5);
let reaturningNums8 = [3,3,7,7,3,3,4,5,5,8,8,8].filter((item) => {return item === 8}).length;
console.log(reaturningNums8);

const falsyArray = [NaN,0,false,77,-14,undefined,null,5,"",5,8,8,8];
const filteredResult = falsyArray.filter(item => !!item);
console.log(filteredResult);
